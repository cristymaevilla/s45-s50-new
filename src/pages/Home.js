import {Fragment} from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
// import Coursecard from '../components/Coursecard';

export default function Home(){
	return(
		<Fragment>
			<Banner/>
			<Highlights/>

		</Fragment>
		)
		
	
}